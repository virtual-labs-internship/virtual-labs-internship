# Stacks and Queues Virtual Lab Review
Author: Max Greenspan

Date: 3 June 2022

Experiment Link: [Stacks and Queues](https://ds1-iiith.vlabs.ac.in/exp/stacks-queues/index.html)
---
## Critical Review:
The UI color scheme and interaction elements were identical to that of other labs, except in the issues noted below.

The usability was less than I had expected, since there were a few technical issues and discrepencies, as noted below.

The performance of the lab was great, there was no lag or latency, and the only "performance" issue were the technical issues.

The content structure was clear, but the overview video seemed outdated, and the exercises in both the Stack and Queue sections were not as beneficial to learning Stacks and Queues as I had hoped, and this the content quality was diminished. That being said, the other sections were great.

## The improvements that I would recommend are as follows:

_Stacks and Queue overview video_:
The overview video breaks down each component of the lab, but the UI in the video is different than what currently is displayed in the lab. The issue lies in the name of each section in the content tab, since many of them are different than those that appear in the video.
For instance: the "Recap: Arrays", "Recap: Linked List", and "Pretest" sections are all shown separately in the current version of the lab, but in the video they appear as subsections of a section titled Pre-Test. And for the "Stack" section, most of the subheaders are different than that of the current UI. I assume "Stacks: Basics" from the video corresponds to the "Concept" section, but it is hard to tell. Also, nearly all of the "Queue" subsections are titled: "Queues: Name of Section" instead of just the name of the section, which differs from the procedure of the prior Stack section, which doesn't list "Stack:" in front of every subheader name. These discrepancies make the overview video, which aims to simplify the learners task of traversing the lab, confusing.

_Pretest_:
The pretest did not explain why certain answers we're wrong. It would be beneficial to explain the potential flaws in ones answers to help the user better understand their gaps of understanding of the underlying topics of Arrays and Linked Lists, which are necessary and fundamental to learning Stacks and Queues.

_Stacks and Queues Time Complexity Page_:
The time complexities of Insertion, Deletion, and Search are listed out, but I believe that they can be formatted better. Each action should begin on a new line as opposed to the current paragraph format. This way the distinction between each one is more clear.

_Stacks Linked List Practice Page_:
The UI was full of bugs, and wouldn't run properly. I could only push at most two elements into the stack, the first one would cause a weird arrow movement accross the screen, and the second one displayed no event. Once two elements were pushed, the pop button wouldn't pop the elements, when used. These visual issues were identical when I tested them on Safari, Chrome, and Firefox, and also occured in the same manner on the "Queues: Linked List Practice" section aswell.

_Stacks Exercise Section_:
The directions read: "Question: A set of elements are given in Stack A. With the help of Stack B, POP the elements in ASCENDING order."
It might be helpful to put a label below Stack A, and label below stack B. This is because the directions can seem confusing since there is only one stack present as the other is initially empty, and neither of them are labeled. A simple label underneath each stack reading "Stack A", and "Stack B" would clarify this potential confusion. It would also improve UI consistency since this same feature occurs in "Queues: Exercise" section.



